#include <Wire.h>
#include "OneWire.h"

#define DS2413_FAMILY_ID 0x3A
#define DS2413_ACCESS_READ 0xF5
#define DS2413_ACCESS_WRITE 0x5A
#define DS2413_ACK_SUCCESS 0xAA
#define DS2413_ACK_ERROR 0xFF

OneWire oneWire;

uint8_t currAddress[8] = {0, 0, 0, 0, 0, 0, 0, 0};
uint8_t curState = 1;

void printAddress(uint8_t deviceAddress[8])
{
  Serial.print("{ ");
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    Serial.print("0x");
    if (deviceAddress[i] < 16)
      Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
    if (i < 7)
      Serial.print(", ");
  }
  Serial.print(" }");
}



bool write(uint8_t state)
{
  uint8_t ack = 0;

  /* Top six bits must '1' */
  state |= 0xFC;

  oneWire.reset();
  oneWire.select(currAddress);
  oneWire.write(DS2413_ACCESS_WRITE);
  oneWire.write(state);
  oneWire.write(~state); /* Invert data and resend     */
  ack = oneWire.read();  /* 0xAA=success, 0xFF=failure */
  if (ack == DS2413_ACK_SUCCESS)
  {
    oneWire.read(); /* Read the status byte      */
  }
  oneWire.reset();

  return (ack == DS2413_ACK_SUCCESS ? true : false);
}

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  Serial.println("Checking for I2C devices...:");
  if (oneWire.checkPresence())
  {
    Serial.println("DS2482-100 present");

    oneWire.deviceReset();

    Serial.println("\tChecking for 1-Wire devices...");
    if (oneWire.wireReset())
    {
      Serial.println("\tDevices present on 1-Wire bus");

      // uint8_t currAddress[8];

      Serial.println("\t\tSearching 1-Wire bus...");

      curState = 1 - curState;
      while (oneWire.wireSearch(currAddress))
      {
        Serial.print("\t\t\tFound device: ");
        printAddress(currAddress);
        if (curState == 1)
        {
          write(0x3);
        }
        else
        {
          write(0x0);
        }

        Serial.println();
      }

      oneWire.wireResetSearch();
    }
    else
      Serial.println("\tNo devices on 1-Wire bus");
  }
  else
    Serial.println("No DS2482-100 present");

  delay(3000);
}
